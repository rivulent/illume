<?php
/**
 * Created by PhpStorm.
 * User: cjmcd
 * Date: 1/11/2019
 * Time: 9:10 AM
 */
declare(strict_types=1);


namespace Rivulent\Illume\Tests;


use \PHPUnit\Framework\TestCase;
use Rivulent\Illume\Illume;
use Rivulent\Illume\IllumeApiNotFoundException;
use Rivulent\Illume\IllumeMissingParameterException;

final class IllumeTest extends TestCase
{
    /**
     * @test
     */
    public function missing_params_throws_exception()
    {
        $this->expectException(IllumeMissingParameterException::class);

        new Illume(null, null, null);
    }

    /**
     * @test
     */
    public function missing_url_throws_exception()
    {
        $this->expectException(IllumeMissingParameterException::class);

        new Illume(null, 'consumer_key', 'consumer_secret');
    }

    /**
     * @test
     * @throws IllumeMissingParameterException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function missing_consumer_key_throws_exception()
    {
        $this->expectException(IllumeMissingParameterException::class);

        new Illume('http://www.myserver.com/designerservice/api/', null, 'consumer_secret');
    }

    /**
     * @test
     */
    public function missing_consumer_secret_throws_exception()
    {
        $this->expectException(IllumeMissingParameterException::class);

        new Illume('http://www.myserver.com/designerservice/api/', 'consumer_key', null);
    }

    /**
     * @test
     */
    public function url_pulled_from_environment()
    {
        $url = 'http://www.myserver.com/designerservice/api/';
        putenv('ILLUME_URL=' . $url);

        $illume = new Illume(null, 'consumer_key', 'consumer_secret');

        $this->assertEquals($url, $illume->getBaseUrl());

        putenv('ILLUME_URL');
    }

    /**
     * @test
     */
    public function consumer_key_pulled_from_environment()
    {
        $key = 'consumer_key';
        putenv('ILLUME_KEY=' . $key);

        $illume = new Illume('http://www.myserver.com/designerservice/api/', null, 'consumer_secret');

        $this->assertEquals($key, $illume->getConsumerKey());

        putenv('ILLUME_KEY');
    }

    /**
     * @test
     */
    public function consumer_secret_pulled_from_environment()
    {
        $secret = 'consumer_secret';
        putenv('ILLUME_SECRET=' . $secret);

        $illume = new Illume('http://www.myserver.com/designerservice/api/', 'consumer_key', null);

        $this->assertEquals($secret, $illume->getConsumerSecret());

        putenv('ILLUME_SECRET');
    }

    /**
     * @test
     */
    public function url_and_consumer_key_and_consumer_secret_pulled_from_environment()
    {
        $url = 'http://www.myserver.com/designerservice/api/';
        putenv('ILLUME_URL=' . $url);

        $key = 'consumer_key';
        putenv('ILLUME_KEY=' . $key);

        $secret = 'consumer_secret';
        putenv('ILLUME_SECRET=' . $secret);

        $illume = new Illume(null, null, null);

        $this->assertEquals($url, $illume->getBaseUrl());
        $this->assertEquals($key, $illume->getConsumerKey());
        $this->assertEquals($secret, $illume->getConsumerSecret());

        putenv('ILLUME_URL');
        putenv('ILLUME_KEY');
        putenv('ILLUME_SECRET');
    }

    /**
     * @test
     */
    public function parameters_preferred_over_environment()
    {
        $url = 'http://www.myserver.com/designerservice/api/';
        putenv('ILLUME_URL=http://www.yourserver.com/designerservice/api/');

        $key = 'consumer_key';
        putenv('ILLUME_KEY=consumer_lock');

        $secret = 'consumer_secret';
        putenv('ILLUME_SECRET=consumer_obvious');

        $illume = new Illume($url, $key, $secret);

        $this->assertEquals($url, $illume->getBaseUrl());
        $this->assertEquals($key, $illume->getConsumerKey());
        $this->assertEquals($secret, $illume->getConsumerSecret());

        putenv('ILLUME_URL');
        putenv('ILLUME_KEY');
        putenv('ILLUME_SECRET');
    }

    /**
     * @test
     */
    public function missing_api_throws_api_not_found_exception()
    {
        $this->expectException(IllumeApiNotFoundException::class);

        $url = 'http://www.myserver.com/designerservice/api/';
        $key = 'consumer_key';
        $secret = 'consumer_secret';

        $illume = new Illume($url, $key, $secret);
        $illume->login('username', 'password');
    }

    /**
     * @test
     */
    public function missing_login_params_throws_exception()
    {
        $this->expectException(IllumeMissingParameterException::class);

        $url = 'http://www.myserver.com/designerservice/api/';
        $key = 'consumer_key';
        $secret = 'consumer_secret';

        $illume = new Illume($url, $key, $secret);
        $illume->login(null, null);
    }

    /**
     * @test
     */
    public function missing_username_throws_exception()
    {
        $this->expectException(IllumeMissingParameterException::class);

        $url = 'http://www.myserver.com/designerservice/api/';
        $key = 'consumer_key';
        $secret = 'consumer_secret';

        $illume = new Illume($url, $key, $secret);
        $illume->login(null, 'password');
    }

    /**
     * @test
     */
    public function missing_password_throws_exception()
    {
        $this->expectException(IllumeMissingParameterException::class);

        $url = 'http://www.myserver.com/designerservice/api/';
        $key = 'consumer_key';
        $secret = 'consumer_secret';

        $illume = new Illume($url, $key, $secret);
        $illume->login('username', null);
    }
}