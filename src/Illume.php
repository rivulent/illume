<?php
/**
 * Created by PhpStorm.
 * User: cjmcd
 * Date: 1/11/2019
 * Time: 7:25 AM
 */
declare(strict_types=1);


namespace Rivulent\Illume;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;
use Safe\Exceptions\ArrayException;
use Safe\Exceptions\JsonException;
use function Safe\json_decode;
use function Safe\ksort;

final class Illume
{
    /**
     * Retrieved from api
     *
     * @var string $accessToken
     */
    protected $accessToken;

    /**
     * Retrieved from api
     *
     * @var string $accessTokenSecret
     */
    protected $accessTokenSecret;

    /**
     * designerServiceUrl
     *
     * @var string $baseUrl
     */
    protected $baseUrl;

    /**
     * illumeKey
     *
     * @var string $consumerKey
     */
    protected $consumerKey;

    /**
     * illumeSecret
     *
     * @var string $consumerSecret
     */
    protected $consumerSecret;

    /**
     * Guzzle client for reuse, public to allow for one-off requests
     *
     * @var Client $client
     */
    public $client;

    /**
     * illumeSecret
     *
     * @var string $oauthConsumerPrivate
     */
    protected $oauthConsumerPrivate;

    /**
     * XAuth / OAuth params
     *
     * @var array $params
     */
    protected $params = [
        'x_auth_mode' => 'client_auth',
        'oauth_signature_method' => 'HMAC-SHA1',
        'oauth_version' => '1.0'
    ];

    /**
     * User credentials
     *
     * @var string $password
     */
    protected $password;

    /**
     * User credentials
     *
     * @var string $username
     */
    protected $username;

    protected $participantLists = [];

    protected $surveys = [];

    /**
     * Illume constructor.
     *
     * @param string|null $baseUrl
     * @param string|null $consumerKey
     * @param string|null $consumerSecret
     * @throws IllumeMissingParameterException
     * @throws GuzzleException
     */
    public function __construct(string $baseUrl = null, string $consumerKey = null, string $consumerSecret = null)
    {
        if ($baseUrl === null && getenv('ILLUME_URL') !== false) {
            $baseUrl = getenv('ILLUME_URL');
        }

        if ($consumerKey === null && getenv('ILLUME_KEY') !== false) {
            $consumerKey = getenv('ILLUME_KEY');
        }

        if ($consumerSecret === null && getenv('ILLUME_SECRET') !== false) {
            $consumerSecret = getenv('ILLUME_SECRET');
        }

        if (!isset($baseUrl)) {
            throw new IllumeMissingParameterException('Missing Illume parameter $baseUrl');
        }
        if (!isset($consumerKey)) {
            throw new IllumeMissingParameterException('Missing Illume parameter $consumerKey');
        }
        if (!isset($consumerSecret)) {
            throw new IllumeMissingParameterException('Missing Illume parameter $consumerSecret');
        }

        $this->params['oauth_consumer_key'] = $consumerKey;
        $this->params['oauth_nonce'] = md5(random_bytes(32));
        $this->params['oauth_timestamp'] = time();

        $this->oauthConsumerPrivate = $consumerSecret;
        $this->baseUrl = $baseUrl;
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;

        $this->client = new Client([
            'base_uri' => $this->baseUrl,
            'timeout' => 2,
        ]);
    }

    /**
     * Log user in to Illume API
     *
     * @param $username
     * @param $password
     * @return int
     * @throws ArrayException
     * @throws GuzzleException
     * @throws IllumeApiNotFoundException
     * @throws IllumeMissingParameterException
     */
    public function login($username = null, $password = null)
    {
        if ($username === null && getenv('ILLUME_USERNAME') !== false) {
            $username = getenv('ILLUME_USERNAME');
        }

        if ($password === null && getenv('ILLUME_PASSWORD') !== false) {
            $password = getenv('ILLUME_PASSWORD');
        }

        if (!isset($username)) {
            throw new IllumeMissingParameterException('Missing Illume parameter $username');
        }
        if (!isset($password)) {
            throw new IllumeMissingParameterException('Missing Illume parameter $password');
        }

        // verify api is up
        try {
            $this->client->request('GET');
        }
        catch (ConnectException $e) {
            // networking error
            throw new IllumeApiNotFoundException($e->getMessage());
        }
        catch (ClientException $e) {
            // 400 level errors
            throw new IllumeApiNotFoundException($e->getMessage());
        }
        catch (RequestException $e) {
            // networking error (connection timeout, DNS errors, etc.)
            throw new IllumeApiNotFoundException($e->getMessage());
        }

        $this->params['x_auth_username'] = $username;
        $this->params['x_auth_password'] = $password;
        $this->generate_signature($this->baseUrl . 'oauth/access_token');

        $response = $this->client->request('POST', 'oauth/access_token', [
            'form_params' => $this->params,
        ]);

        $body = (string) $response->getBody();
        parse_str($body, $tokens);

        $this->accessToken = $tokens['oauth_token'];
        $this->accessTokenSecret = $tokens['oauth_token_secret'];

        $stack = HandlerStack::create();

        $middleware = new Oauth1([
            'consumer_key'    => $this->consumerKey,
            'consumer_secret' => $this->consumerSecret,
            'token'           => $this->accessToken,
            'token_secret'    => $this->accessTokenSecret,
        ]);
        $stack->push($middleware);

        $this->client = new Client([
            'base_uri' => $this->baseUrl,
            'handler' => $stack,
            'auth' => 'oauth'
        ]);

        return $response->getStatusCode();
    }

    public function addParticipantToList($listName, $participant)
    {
        //$list = $this->getParticipantList($listName);
        //
        //$response = $this->client->request('POST', ltrim($list->Uri, '/'), [
        //    'json' => $participant
        //]);
        //
        //return $response->getStatusCode();
    }

    public function deleteParticipantFromList($participant)
    {
        //$response = $this->client->request('DELETE', ltrim($participant->Uri, '/'));
        //
        //return $response->getStatusCode();
    }

    public function deleteParticipantSubmissions($surveyName, $identifier, $identifierVariable = 'DATSTAT_ALTPID')
    {
        //$survey = $this->getSurvey($surveyName);
        //
        //$queryUrl = $this->createResultQuery($identifier, $identifierVariable);
        //
        //$response = $this->client->request('GET', ltrim($survey->Uri, '/') . '/results?query=' . $queryUrl);
        //$data = json_decode((string) $response->getBody())->Data;
        //
        //foreach ($data as $key => $session) {
        //    $this->client->request('DELETE', 'surveysessions/' . $session[0]);
        //}

        return false;
    }

    /**
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->baseUrl;
    }

    /**
     * @return string
     */
    public function getConsumerKey()
    {
        return $this->consumerKey;
    }

    /**
     * @return string
     */
    public function getConsumerSecret()
    {
        return $this->consumerSecret;
    }

    /**
     * Get participant object from participant list
     *
     * This is a multi-step process that requires creating a query, applying the query to the participant list,
     * then retrieving the participant object
     *
     * @param $listName
     * @param $identifier
     * @param null $identifierVariable
     * @return mixed
     * @throws IllumeParticipantListNotFoundException
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getParticipantFromParticipantList($listName, $identifier, $identifierVariable = 'DATSTAT_ALTPID')
    {
        $list = $this->GetParticipantList($listName);

        $queryUrl = $this->createParticipantQuery($identifier, $identifierVariable);

        $response = $this->client->request('GET', ltrim($list->Uri, '/') . '/participants?query=' . $queryUrl, ['debug' => true]);
        $participantRecord = json_decode((string) $response->getBody())[0];

        $response = $this->client->request('GET', ltrim($participantRecord->Uri, '/'));
        $participant = json_decode((string) $response->getBody());

        return $participant;
    }

    /**
     * Get single participant list by participant list description (name)
     *
     * @param null $listName
     * @return object
     * @throws IllumeParticipantListNotFoundException
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getParticipantList($listName)
    {
        if (count($this->participantLists) === 0) {
            $this->getParticipantLists();
        }

        foreach ($this->participantLists as $list) {
            if ($listName === $list->Description) {
                return $list;
            }
        }

        throw new IllumeParticipantListNotFoundException();
    }

    /**
     * Returns array of Participant List objects
     *
     * @return array
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getParticipantLists()
    {
        if (count($this->participantLists) > 0) {
            return $this->participantLists;
        }

        $response = $this->client->request('GET', 'participantlists');
        $json = json_decode((string) $response->getBody());
        $this->participantLists = $json;

        return $this->participantLists;
    }

    /**
     * Returns all data for a single submission for a participant
     *
     * @param $sessionId
     * @return mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getResponsesBySessionId($sessionId)
    {
        $response = $this->client->request('GET', 'surveysessions/' . $sessionId);
        $json = json_decode((string) $response->getBody());

        return $json;
    }

    /**
     * Returns array of Surveys objects
     *
     * @param $surveyName
     * @return object
     * @throws IllumeParticipantListNotFoundException
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getSurvey($surveyName)
    {
        if (count($this->surveys) === 0) {
            $this->getSurveys();
        }

        foreach ($this->surveys as $survey) {
            if ($surveyName === $survey->Description) {
                return $survey;
            }
        }

        throw new IllumeParticipantListNotFoundException();
    }

    /**
     * Returns array of Surveys objects
     *
     * @return array
     * @throws GuzzleException
     * @throws JsonException
     */
    public function getSurveys()
    {
        if (count($this->surveys) > 0) {
            return $this->surveys;
        }

        $response = $this->client->request('GET', 'illumesurveys');
        $json = json_decode((string) $response->getBody());
        $this->surveys = $json;

        return $this->surveys;
    }

    public function updateParticipant($participant)
    {
        //$response = $this->client->request('POST', ltrim($participant->Uri, '/'), [
        //    'json' => $participant
        //]);
        //
        //return $response->getStatusCode();
    }

    /**
     * Create a reusable participant query
     *
     * @param $identifier
     * @param null $identifierVariable
     * @return mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    private function createParticipantQuery($identifier, $identifierVariable)
    {
        $params = [
            'Filters' => [
                "A" => [
                    'Variable' => $identifierVariable,
                    'QueryOperator' => 2,
                    'Value' => $identifier
                ]
            ],
            'headers'
        ];

        $response = $this->client->request('POST', 'participantqueries/new', [
            'json' => $params,
        ]);

        $json = json_decode((string) $response->getBody());
        return $json->Uri;
    }

    /**
     * Create a reusable participant query
     *
     * @param $identifier
     * @param null $identifierVariable
     * @return mixed
     * @throws GuzzleException
     * @throws JsonException
     */
    private function createResultQuery($identifier, $identifierVariable)
    {
        $params = [
            'IncludeRawData' => true,
            'IncludePartialData' => false,
            'Variables' => [
                'DATSTAT.SESSIONID'
            ],
            'Filters' => [
                'A' => [
                    'Variable' => $identifierVariable,
                    'Value' => $identifier,
                    'QueryOperator' => 2,
                ]
            ],
        ];

        $response = $this->client->request('POST', 'resultqueries/new', [
            'json' => $params,
        ]);

        $json = json_decode((string) $response->getBody());

        return $json->Uri;
    }

    /**
     * @param $string
     * @return bool|string
     */
    private function encode($string)
    {
        if ($string === false) {
            return false;
        }

        return str_replace('%7E', '~', rawurlencode((string) $string));
    }

    /**
     * @param $accessUrl
     * @throws ArrayException
     */
    private function generate_signature($accessUrl)
    {
        $oauth_params = $this->generate_oauth_params();
        $signature_base = 'POST&' . $this->encode($accessUrl) . '&' . $oauth_params;
        $key = $this->oauthConsumerPrivate . '&';
        $this->params['oauth_signature'] = base64_encode(hash_hmac("sha1", $signature_base, $key, true));
    }

    /**
     * @return string
     * @throws ArrayException
     */
    private function generate_oauth_params()
    {
        ksort($this->params);
        $req = array();
        foreach ($this->params as $key => $value) {
            $req[] = $this->encode($key) . "%3D" . $this->encode($this->encode($value));
        }
        return implode("%26", $req);
    }
}