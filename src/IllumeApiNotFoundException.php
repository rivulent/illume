<?php
/**
 * Created by PhpStorm.
 * User: cjmcd
 * Date: 1/14/2019
 * Time: 12:42 PM
 */
declare(strict_types=1);


namespace Rivulent\Illume;

use Exception;

final class IllumeApiNotFoundException extends Exception
{

}